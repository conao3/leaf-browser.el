#+author: conao
#+date: <2019-01-04 Fri>

[[https://github.com/conao3/leaf-browser.el][https://img.shields.io/github/tag/conao3/leaf-browser.el.svg?style=flat-square]]
[[https://travis-ci.org/conao3/leaf-browser.el][https://img.shields.io/travis/conao3/leaf-browser.el/master.svg?style=flat-square]]
[[https://github.com/conao3/leaf-browser.el][https://img.shields.io/github/license/conao3/leaf-browser.el.svg?style=flat-square]]
[[https://github.com/conao3/github-header][https://files.conao3.com/github-header/gif/leaf-browser.el.gif]]

- [[What is it?]]

* What is it?
~leaf-browser.el~ provide Web frontend of Emacs custom-mode.

* License of dependent software
** jquery/jquery
This package depend on [[https://github.com/jquery/jquery][jquery/jquery]].
It is distributed under the MIT License.

#+begin_quote
The MIT License (MIT)

Copyright JS Foundation and other contributors, https://js.foundation/

https://github.com/jquery/jquery/blob/master/LICENSE.txt
#+end_quote

** Dogfalo/materialize
This package depend on [[https://github.com/Dogfalo/materialize][Dogfalo/materialize]].
It is distributed under the MIT License.

#+begin_quote
The MIT License (MIT)

Copyright (c) 2014-2018 Materialize

https://github.com/Dogfalo/materialize/blob/v1-dev/LICENSE
#+end_quote

** tholman/github-corners
This package depend on [[https://github.com/tholman/github-corners/blob/master/license.md][tholman/github-corners]].
It is distributed under the MIT License.

#+begin_quote
The MIT License (MIT)

Copyright (c) 2016 Tim Holman - http://tholman.com

https://github.com/tholman/github-corners/blob/master/license.md
#+end_quote
